package ru.ckateptb.worldreset;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class WorldReset extends JavaPlugin {

    public void onLoad() {
        this.saveDefaultConfig();
        WRWorldManager wm = new WRWorldManager(this);
        boolean backupsFound = false;
        File backupDir = new File(this.getDataFolder(), "backups");
        if (!backupDir.exists()) {
            backupDir.mkdirs();
        }

        if (this.getConfig().getBoolean("enable")) {
            getLogger().info("Looking for world backups in '.../plugins/WorldReset/backups'");
            File[] arrayOfFile;
            int j = (arrayOfFile = backupDir.listFiles()).length;
            for (int i = 0; i < j; ++i) {
                File backup = arrayOfFile[i];
                if (backup.isDirectory() && backup.listFiles().length != 0) {
                    backupsFound = true;
                }
            }
            if (backupsFound) {
                wm.importWorlds();
                getLogger().info("World transfer complete.");
            }
        }
    }

    public void onEnable() {
        this.getCommand("worldreset").setExecutor(new WRCommand(this));
    }
}
