package ru.ckateptb.worldreset;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class WRCommand implements CommandExecutor {
    private WorldReset plugin;

    public WRCommand(WorldReset plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1 && sender.hasPermission("worldreset.use")) {
            this.plugin.getConfig().set("enable", Boolean.valueOf(args[0]));
        }
        return true;
    }
}