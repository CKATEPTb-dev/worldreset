package ru.ckateptb.worldreset;

import java.io.*;

public class WRWorldManager {
    private WorldReset plugin;

    public WRWorldManager(final WorldReset wr) {
        super();
        this.plugin = wr;
    }

    private static void copyDir(final File source, final File target) throws IOException {
        if (source.isDirectory()) {
            if (!target.exists()) {
                target.mkdir();
            }
            final String[] files = source.list();
            String[] arrayOfString1;
            for (int j = (arrayOfString1 = files).length, i = 0; i < j; ++i) {
                final String file = arrayOfString1[i];
                final File srcFile = new File(source, file);
                final File destFile = new File(target, file);
                copyDir(srcFile, destFile);
            }
        } else {
            final InputStream in = new FileInputStream(source);
            final OutputStream out = new FileOutputStream(target);
            final byte[] buffer = new byte[in.available()];
            int length;
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }
            in.close();
            out.close();
        }
    }

    public void importWorlds() {
        boolean errors = false;
        final File backupDir = new File(this.plugin.getDataFolder(), "backups");
        File[] arrayOfFile;
        for (int j = (arrayOfFile = backupDir.listFiles()).length, i = 0; i < j; ++i) {
            final File source = arrayOfFile[i];
            if (source.isDirectory()) {
                final File target = new File(this.plugin.getServer().getWorldContainer(), source.getName() + File.separator + "region");
                if (target.exists() && target.isDirectory() && !this.delete(target)) {
                    this.plugin.getLogger().info("Failed to reset world \"" + source.getName() + "\" - could not delete old world folder.");
                    errors = true;
                } else {
                    try {
                        copyDir(new File(source, "region"), target);
                    } catch (IOException e) {
                        e.printStackTrace();
                        this.plugin.getLogger().info("Failed to reset world \"" + source.getName() + "\" - could not import the world from backup.");
                        errors = true;
                    }
                }
                this.plugin.getLogger().info("Import of world \"" + source.getName() + "\" " + (errors ? "failed!" : "succeeded!"));
            }
        }

    }

    private boolean delete(final File file) {
        if (file.isDirectory()) {
            File[] arrayOfFile;
            for (int j = (arrayOfFile = file.listFiles()).length, i = 0; i < j; ++i) {
                final File subfile = arrayOfFile[i];
                if (!this.delete(subfile)) {
                    return false;
                }
            }
        }
        return file.delete();
    }
}
